const handler = require('../controllers/actors')

module.exports = (router) => {
    const ENDPOINT = '/actors'
    router
        .get(`${ENDPOINT}/:page/:limit`, ctx => handler.handleActorPagination(ctx))
        .get(`${ENDPOINT}/:id`, ctx => handler.handleActorDetails(ctx))
}
