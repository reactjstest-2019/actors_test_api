const { actors } = require('../lib/seeds/index')

exports.handleActorPagination = async ctx => {
    try {
        if (
            ctx.params && ctx.params.page && parseInt(ctx.params.page)
            && ctx.params.limit && parseInt(ctx.params.limit)
        ) {
            const page = parseInt(ctx.params.page)
            const limit = parseFloat(ctx.params.limit)
            const start = (page -1) * limit
            const actors_found = actors.slice(start, start + limit)
            if (actors_found) {
                ctx.status = 200
                ctx.body = {
                    data: { list: actors_found, count: actors.length }
                }
            } else {
                const error = {
                    status: 404,
                    title: 'Not found',
                    message: 'Actor(s) not found'
                }
                ctx.status = error.status
                ctx.body = {
                    error: error
                }
            }
        } else {
            const error = {
                status: 400,
                title: 'Validation failed',
                message: 'Invalid page or limit'
            }
            ctx.status = error.status
            ctx.body = {
                error: error
            }
        }
    } catch (err) {
        throw new Error(err.message)
    }
}
exports.handleActorDetails = async ctx => {
    try {
        if (ctx.params && ctx.params.id && parseInt(ctx.params.id)) {
            const { id } = ctx.params
            const actor = actors.find(item => item.id == id)
            if (actor && actor.id) {
                ctx.status = 200
                ctx.body = {
                    data: { actor, count: actors.length }
                }
            } else {
                const error = {
                    status: 404,
                    title: 'Not found',
                    message: 'Actor not found'
                }
                ctx.status = error.status
                ctx.body = {
                    error: error
                }
            }
        } else {
            const error = {
                status: 400,
                title: 'Validation failed',
                message: 'ID is not valid'
            }
            ctx.status = error.status
            ctx.body = {
                error: error
            }
        }
    } catch (err) {
        throw new Error(err.message)
    }
}
