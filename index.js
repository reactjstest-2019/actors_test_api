const Koa = require('koa');
const cors = require('@koa/cors');
const { SERVER_PORT } = require('./constants/index');
const generalErrorHandler = require('./lib/middleware/error_handler');
const setRoutes = require('./routes');
// server info
const PORT = SERVER_PORT || 4000;

const app = new Koa();

app.use(cors());
// general exception handler
app.use(generalErrorHandler);
// setup routes
setRoutes(app);
// bootstrap the app
const server = app.listen(PORT, () => {
    console.log(`app is running on port ${PORT}`);
});

module.exports = {
    server,
};

