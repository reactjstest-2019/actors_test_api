const photos = [
    'https://via.placeholder.com/150/92c952',
    'https://via.placeholder.com/150/771796',
    'https://via.placeholder.com/150/24f355',
    'https://via.placeholder.com/150/d32776',
    'https://via.placeholder.com/150/f66b97',
    'https://via.placeholder.com/150/56a8c2',
    'https://via.placeholder.com/150/b0f7cc',
    'https://via.placeholder.com/150/54176f',
    'https://via.placeholder.com/150/51aa97',
    'https://via.placeholder.com/150/810b14'
]

const actors = []

for (let i = 1; i <=100; i++) {
    actors.push({
        id: i,
        first_name: `first name ${i}`,
        last_name: `last name ${i}`,
        bio: `some bio here ${i} some bio here ${i} some bio here ${i} some bio here ${i} some bio here ${i} some bio here ${i} some bio here ${i} some bio here ${i} some bio here ${i} `,
        age: Math.floor(Math.random() * 100),
        featured_movies: ['some movies', 'some movies', 'some movies'],
        img_url: photos[Math.floor(Math.random() * 10)]
    })
}

exports.actors = actors
